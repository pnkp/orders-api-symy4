<?php

namespace App\Common\Service\User;

use App\Common\Dto\User\UserRequestDto;
use App\Common\Dto\User\UserResponseDto;
use App\Common\Exception\BadRequestException;
use App\Common\Exception\ValidationException;
use App\Common\Validator\ConstraintValidatorInterface;
use App\Repository\User\UserRepositoryInterface;
use Symfony\Component\Serializer\SerializerInterface;

class UserService implements UserServiceInterface
{
    /**
     * @var ConstraintValidatorInterface
     */
    private $validator;
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(
        UserRepositoryInterface $userRepository,
        SerializerInterface $serializer,
        ConstraintValidatorInterface $validator
    )
    {
        $this->validator = $validator;
        $this->userRepository = $userRepository;
        $this->serializer = $serializer;
    }

    public function createUser(UserRequestDto $userRequestDto): UserResponseDto
    {
        $this->validator->addToValidate($userRequestDto);
        $this->validator->validate();

        if (count($this->validator->getAllConstraintViolations()) > 0) {
            throw new ValidationException($this->validator->getAllConstraintViolations(), 400);
        }

        if ($userRequestDto->getRetypedPassword() !== $userRequestDto->getPassword()) {
            throw new BadRequestException(400, ['password' => 'Passwords is not equals']);
        }

        $user = $this->userRepository->create($userRequestDto);

        return new UserResponseDto(
            $user->getId(),
            $user->getUsername(),
            $user->getEmail()
        );
    }
}
