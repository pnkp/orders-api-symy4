<?php

namespace App\Common\Service\User;

use App\Common\Dto\User\UserRequestDto;
use App\Common\Dto\User\UserResponseDto;

interface UserServiceInterface
{
    public function createUser(UserRequestDto $request): UserResponseDto;
}