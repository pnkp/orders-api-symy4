<?php

namespace App\Common\Service\Partner;

use App\Common\Dto\Partner\PartnerRequestDto;
use App\Common\Dto\Partner\PartnerResponseDto;
use App\Common\Dto\Partner\PartnerResponseSuperAdminDto;

interface PartnerServiceInterface
{
    public function create(PartnerRequestDto $partnerRequestDto): PartnerResponseDto;

    public function findPartnerById(int $id): PartnerResponseSuperAdminDto;

    public function findPartnerByCriteria(array $criteria): PartnerResponseSuperAdminDto;
}
