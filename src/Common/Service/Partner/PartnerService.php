<?php

namespace App\Common\Service\Partner;

use App\Common\Dto\Partner\PartnerRequestDto;
use App\Common\Dto\Partner\PartnerResponseDto;
use App\Common\Dto\Partner\PartnerResponseSuperAdminDto;
use App\Common\Exception\ValidationException;
use App\Common\Validator\ConstraintValidatorInterface;
use App\Entity\Partner;
use App\Repository\Partner\PartnerRepositoryInterface;
use Doctrine\ORM\ORMException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PartnerService implements PartnerServiceInterface
{
    /**
     * @var PartnerRepositoryInterface
     */
    private $partnerRepository;
    /**
     * @var ConstraintValidatorInterface
     */
    private $validator;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        PartnerRepositoryInterface $partnerRepository,
        ConstraintValidatorInterface $validator,
        LoggerInterface $logger
    )
    {
        $this->partnerRepository = $partnerRepository;
        $this->validator = $validator;
        $this->logger = $logger;
    }

    public function create(PartnerRequestDto $partnerRequestDto): PartnerResponseDto
    {
        $this->validator->addToValidate($partnerRequestDto);
        $this->validator->validate();

        if (count($this->validator->getAllConstraintViolations()) > 0) {
            throw new ValidationException($this->validator->getAllConstraintViolations(), Response::HTTP_BAD_REQUEST);
        }

        $partner = new Partner();
        $partner->setName($partnerRequestDto->getName());

        $partner = $this->partnerRepository->createPartner($partner);

        $partnerResponseDto = new PartnerResponseDto();
        $partnerResponseDto->setId($partner->getId());
        $partnerResponseDto->setName($partner->getName());

        return $partnerResponseDto;
    }

    public function findPartnerById(int $id): PartnerResponseSuperAdminDto
    {
        $partner = $this->partnerRepository->findOneById($id);

        if (null === $partner) {
            throw new NotFoundHttpException("PARTNER_NOT_FOUND");
        }

        return $this->partnerResponseSuperAdminDtoFactory($partner);
    }

    public function findPartnerByCriteria(array $criteria): PartnerResponseSuperAdminDto
    {
        try {
            $partner = $this->partnerRepository->findOneByCriteria($criteria);
        } catch (ORMException $exception) {
            $this->logger->warning($exception->getMessage(), [$exception]);
            throw new NotFoundHttpException("BAD_REQUEST_QUERY");
        }

        if (null === $partner) {
            throw new NotFoundHttpException("PARTNER_NOT_FOUND");
        }

        return $this->partnerResponseSuperAdminDtoFactory($partner);
    }

    private function partnerResponseSuperAdminDtoFactory(Partner $partner): PartnerResponseSuperAdminDto
    {
        $partnerResponse = new PartnerResponseSuperAdminDto();
        $partnerResponse->setId($partner->getId());
        $partnerResponse->setName($partner->getName());
        $partnerResponse->setUsers($partner->getUsers()->toArray());

        return $partnerResponse;
    }
}
