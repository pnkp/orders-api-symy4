<?php

namespace App\Common\Serializer;

interface SerializerInterface
{
    public function serialize(object $data, string $format = 'json'): string;
}
