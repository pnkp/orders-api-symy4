<?php

namespace App\Common\Validator;

use Symfony\Component\Validator\ConstraintViolationList;

interface ConstraintValidatorInterface
{
    public function addToValidate(object $object): object;

    public function validate(): object;

    public function getAllConstraintViolations(): ConstraintViolationList;
}