<?php

namespace App\Common\Validator\ConstraintValidator\UniqueField;

use Doctrine\Common\Annotations\Annotation\Required;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 *
 * Class ConstraintUniqueField
 * @package App\Common\Validator\ConstraintValidator\UniqueField
 */
class ConstraintUniqueField extends Constraint
{
    public $message = 'The field is not unique';

    /**
     * @Required()
     *
     * @var string
     */
    public $fieldName;

    /**
     * @Required()
     *
     * @var string
     */
    public $entity;

    /**
     * @var bool
     */
    public $deletedAt;
}
