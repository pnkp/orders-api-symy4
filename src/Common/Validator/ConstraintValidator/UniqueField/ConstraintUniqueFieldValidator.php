<?php

namespace App\Common\Validator\ConstraintValidator\UniqueField;

use App\Repository\User\UserRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ConstraintUniqueFieldValidator extends ConstraintValidator
{
    /**
     * @var UserRepositoryInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        /**
         * @var ConstraintUniqueField $constraint
         */
        $query = [$constraint->fieldName => $value];

        if ($constraint->deletedAt) {
            $query['deletedAt'] = null;
        }

        $user = $this->entityManager->getRepository($constraint->entity)->findOneBy($query);

        if (null !== $user) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}
