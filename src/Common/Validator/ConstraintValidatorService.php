<?php

namespace App\Common\Validator;

use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ConstraintValidatorService implements ConstraintValidatorInterface
{
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var ConstraintViolationList
     */
    private $violationList;
    /**
     * Objects to validate
     * @var array of object
     */
    private $objectToValidate = [];

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
        $this->violationList = new ConstraintViolationList();
    }

    public function addToValidate(object $object): object
    {
        $this->objectToValidate[] = $object;
        return $this;
    }

    public function validate(): object
    {
        foreach ($this->objectToValidate as $object) {
            $errors = $this->validator->validate($object);

            if ($errors->count() > 0) {
                foreach ($errors as $error) {
                    $errorValueObject = new ConstraintViolationValueObject($error->getMessage(), $error->getPropertyPath());
                    $this->violationList->add($errorValueObject);
                }
            }
        }
        return $this;
    }

    public function getAllConstraintViolations(): ConstraintViolationList
    {
        return $this->violationList;
    }
}