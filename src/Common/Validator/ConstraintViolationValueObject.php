<?php

namespace App\Common\Validator;

use JsonSerializable;
use Symfony\Component\Validator\ConstraintViolationInterface;

class ConstraintViolationValueObject implements JsonSerializable, ConstraintViolationInterface
{
    private $element;

    private $message;

    public function __construct(string $message, string $element)
    {
        $this->message = $message;
        $this->element = $element;
    }

    public function jsonSerialize()
    {
        return [
            $this->element => $this->message
        ];
    }

    /**
     * Returns the violation message.
     *
     * @return string The violation message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Returns the raw violation message.
     *
     * The raw violation message contains placeholders for the parameters
     * returned by {@link getParameters}. Typically you'll pass the
     * message template and parameters to a translation engine.
     *
     * @return string The raw violation message
     */
    public function getMessageTemplate()
    {
        return '';
    }

    /**
     * Returns the parameters to be inserted into the raw violation message.
     *
     * @return array a possibly empty list of parameters indexed by the names
     *               that appear in the message template
     *
     * @see getMessageTemplate()
     */
    public function getParameters()
    {
        return [];
    }

    /**
     * Returns a number for pluralizing the violation message.
     *
     * For example, the message template could have different translation based
     * on a parameter "choices":
     *
     * <ul>
     * <li>Please select exactly one entry. (choices=1)</li>
     * <li>Please select two entries. (choices=2)</li>
     * </ul>
     *
     * This method returns the value of the parameter for choosing the right
     * pluralization form (in this case "choices").
     *
     * @return int|null The number to use to pluralize of the message
     */
    public function getPlural()
    {
        return null;
    }

    /**
     * Returns the root element of the validation.
     *
     * @return mixed The value that was passed originally to the validator when
     *               the validation was started. Because the validator traverses
     *               the object graph, the value at which the violation occurs
     *               is not necessarily the value that was originally validated.
     */
    public function getRoot()
    {
        // TODO: Implement getRoot() method.
    }

    /**
     * Returns the property path from the root element to the violation.
     *
     * @return string The property path indicates how the validator reached
     *                the invalid value from the root element. If the root
     *                element is a <tt>Person</tt> instance with a property
     *                "address" that contains an <tt>Address</tt> instance
     *                with an invalid property "street", the generated property
     *                path is "address.street". Property access is denoted by
     *                dots, while array access is denoted by square brackets,
     *                for example "addresses[1].street".
     */
    public function getPropertyPath()
    {
        return $this->element;
    }

    /**
     * Returns the value that caused the violation.
     *
     * @return mixed the invalid value that caused the validated constraint to
     *               fail
     */
    public function getInvalidValue()
    {
        return $this->element;
    }

    /**
     * Returns a machine-digestible error code for the violation.
     *
     * @return string|null The error code
     */
    public function getCode()
    {
        // TODO: Implement getCode() method.
    }
}
