<?php

namespace App\Common\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\ConstraintViolationList;
use Throwable;

class ValidationException extends HttpException
{
    public function __construct(ConstraintViolationList $errorsMessages, $code = 400, Throwable $previous = null)
    {
        $message = [];

        foreach ($errorsMessages as $value) {
            $message[$value->getPropertyPath()] = $value->getMessage();
        }

        parent::__construct($code, json_encode($message));
        $this->code = $code;
    }

    public function getStatusCode()
    {
        return $this->code;
    }
}
