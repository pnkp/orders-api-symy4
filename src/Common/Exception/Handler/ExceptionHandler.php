<?php

namespace App\Common\Exception\Handler;

use App\Common\Exception\BadRequestException;
use App\Common\Exception\ValidationException;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ExceptionHandler implements ExceptionHandlerInterface
{
    public function handle(Exception $exception): Response
    {
        if (in_array(get_class($exception), $this->exceptionsToHandle())) {
            if (!empty($exception->getMessage())) {
                $message = ['error' => $this->getErrorMessage($exception)];
            }

            return new JsonResponse($message ?? null, $exception->getStatusCode());
        }

        if (getenv('APP_ENV') === 'dev') {
            dump($exception);
            die;
        }

        return new JsonResponse(['error' => 'Server internal error'], 500);
    }

    public function exceptionsToHandle(): array
    {
        return [
            ValidationException::class,
            HttpException::class,
            BadRequestException::class,
            AccessDeniedHttpException::class,
            NotFoundHttpException::class
        ];
    }

    private function getErrorMessage(Exception $exception)
    {
        if (is_array(json_decode($exception->getMessage(), true))) {
            return json_decode($exception->getMessage(), true);
        }

        return $exception->getMessage();
    }
}
