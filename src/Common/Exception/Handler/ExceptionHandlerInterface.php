<?php

namespace App\Common\Exception\Handler;

use Exception;
use Symfony\Component\HttpFoundation\Response;

interface ExceptionHandlerInterface
{
    public function handle(Exception $exception): Response;

    public function exceptionsToHandle(): array;
}