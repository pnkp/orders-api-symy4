<?php

namespace App\Common\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

class BadRequestException extends HttpException
{
    /**
     * @var int
     */
    private $statusCode;

    public function __construct(int $statusCode, array $message = null)
    {
        parent::__construct($statusCode, json_encode($message));
        $this->statusCode = $statusCode;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }
}