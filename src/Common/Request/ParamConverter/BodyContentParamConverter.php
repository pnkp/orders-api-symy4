<?php

namespace App\Common\Request\ParamConverter;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BodyContentParamConverter implements ParamConverterInterface
{
    private $serializer;

    private $validator;

    public function __construct(SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    /**
     * Stores the object in the request.
     *
     * @param Request $request
     * @param ParamConverter $configuration Contains the name, class and options of the object
     *
     * @return bool True if the object has been successfully set, else false
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        if (!in_array($request->getContentType(), ['json'])) {
            return false;
        }

        $deserializeObject = $this->serializer->deserialize(
            $request->getContent(),
            $configuration->getClass(),
            $request->getContentType()
        );

        $request->attributes->set($configuration->getName(), $deserializeObject);
        return true;
    }

    /**
     * Checks if the object is supported.
     *
     * @param ParamConverter $configuration
     * @return bool True if the object is supported, else false
     */
    public function supports(ParamConverter $configuration)
    {
        if (empty($configuration->getClass()) && !class_exists($configuration->getClass())) {
            return false;
        }

        if (!$configuration->getName() === null) {
            return false;
        }

        return true;
    }

}
