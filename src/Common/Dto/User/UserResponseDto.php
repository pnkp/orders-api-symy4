<?php

namespace App\Common\Dto\User;

class UserResponseDto
{
    /**
     * @var string
     */
    private $username;
    /**
     * @var string
     */
    private $email;
    /**
     * @var int
     */
    private $id;
    /**
     * @var array
     */
    private $roles;

    public function __construct(
        int $id,
        string $username,
        string $email,
        array $roles
    )
    {
        $this->username = $username;
        $this->email = $email;
        $this->id = $id;
        $this->roles = $roles;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
}
