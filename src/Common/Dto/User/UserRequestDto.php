<?php

namespace App\Common\Dto\User;

use App\Common\Validator\ConstraintValidator\UniqueField\ConstraintUniqueField;
use Symfony\Component\Validator\Constraints as Assert;

class UserRequestDto
{
    /**
     * @Assert\NotBlank()
     * @var string
     */
    private $username;

    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     * @ConstraintUniqueField(fieldName="email", entity="App\Entity\User", deletedAt=true)
     *
     * @var string
     */
    private $email;

    /**
     * @Assert\NotBlank()
     * @var string
     */
    private $password;

    /**
     * @Assert\NotBlank()
     * @var string
     */
    private $retypedPassword;

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getRetypedPassword(): string
    {
        return $this->retypedPassword;
    }

    /**
     * @param string $retypedPassword
     */
    public function setRetypedPassword(string $retypedPassword): void
    {
        $this->retypedPassword = $retypedPassword;
    }
}
