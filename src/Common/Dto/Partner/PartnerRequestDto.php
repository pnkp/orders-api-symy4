<?php

namespace App\Common\Dto\Partner;

use App\Common\Validator\ConstraintValidator\UniqueField\ConstraintUniqueField;
use Symfony\Component\Validator\Constraints as Assert;

class PartnerRequestDto
{
    /**
     * @Assert\NotBlank()
     * @ConstraintUniqueField(fieldName="name", entity="App\Entity\Partner", deletedAt=true)
     * @var string
     */
    private $name;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
