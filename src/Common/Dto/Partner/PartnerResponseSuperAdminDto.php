<?php

namespace App\Common\Dto\Partner;

class PartnerResponseSuperAdminDto extends PartnerResponseDto
{
    /**
     * @var array
     */
    private $users;

    /**
     * @return array
     */
    public function getUsers(): ?array
    {
        return $this->users;
    }

    /**
     * @param array $users
     */
    public function setUsers(array $users): void
    {
        $this->users = $users;
    }
}
