<?php

namespace App\Common\DateTime;

use DateTime;
use DateTimeZone;

class DateTimeUtc
{
    public static function dateTime($time = "now"): DateTime
    {
        return new DateTime($time, new DateTimeZone("UTC"));
    }
}
