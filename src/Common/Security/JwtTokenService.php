<?php

namespace App\Common\Security;

use App\Common\Dto\User\UserResponseDto;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class JwtTokenService implements JwtTokenServiceInterface
{
    /**
     * @var JWTEncoderInterface
     */
    private $JWTEncoder;
    /**
     * @var UserPasswordEncoderInterface c
     */
    private $userPasswordEncoder;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        JWTEncoderInterface $JWTEncoder,
        UserPasswordEncoderInterface $userPasswordEncoder,
        EntityManagerInterface $entityManager
    )
    {
        $this->JWTEncoder = $JWTEncoder;
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->entityManager = $entityManager;
    }

    /**
     * @param Request $request
     * @return JwtTokenValueObject
     * @throws JWTEncodeFailureException
     */
    public function createToken(Request $request): object
    {
        $userEmail = $request->getUser();

        /**
         * @var $user User
         */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['email' => $userEmail]);

        if (!$user) {
            throw new BadCredentialsException();
        }

        if (!$this->userPasswordEncoder->isPasswordValid($user, $request->getPassword())) {
            throw new BadCredentialsException();
        }

        $tokenExpr = time() + (int)(getenv('JWT_TOKEN_EXPR'));

        $token = $this->JWTEncoder->encode([
            'email' => $user->getEmail(),
            'username' => $user->getUsername(),
            'id' => $user->getId(),
            'expr' => $tokenExpr
        ]);

        return new JwtTokenValueObject(
            $token,
            getenv('JWT_TOKEN_TYPE_NAME'),
            $tokenExpr,
            new UserResponseDto(
                $user->getId(),
                $user->getUsername(),
                $user->getEmail(),
                $user->getRoles()
            )
        );
    }
}
