<?php

namespace App\Common\Security;

use App\Common\Dto\User\UserResponseDto;
use App\Entity\User;

class JwtTokenValueObject
{
    /**
     * @var string
     */
    private $token;
    /**
     * @var string
     */
    private $tokenType;
    /**
     * @var int
     */
    private $tokenExp;
    /**
     * @var User
     */
    private $user;

    public function __construct(
        string $token,
        string $tokenType,
        int $tokenExp,
        UserResponseDto $user
    )
    {
        $this->token = $token;
        $this->tokenType = $tokenType;
        $this->tokenExp = $tokenExp;
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getTokenType(): string
    {
        return $this->tokenType;
    }

    /**
     * @return int
     */
    public function getTokenExp(): int
    {
        return $this->tokenExp;
    }

    /**
     * @return UserResponseDto
     */
    public function getUser(): UserResponseDto
    {
        return $this->user;
    }

}