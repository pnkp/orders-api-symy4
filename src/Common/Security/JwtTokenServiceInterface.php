<?php

namespace App\Common\Security;

use Symfony\Component\HttpFoundation\Request;

interface JwtTokenServiceInterface
{
    /**
     * @param Request $request
     * @return JwtTokenValueObject
     */
    public function createToken(Request $request): object;
}