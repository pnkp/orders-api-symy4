<?php

namespace App\Controller\Generic\Security;

use App\Common\Security\JwtTokenServiceInterface;
use App\Common\Serializer\SerializerInterface;
use App\Controller\AbstractBaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("is_anonymous()")
 *
 * Class TokenController
 * @package App\Controller\Generic\Security
 */
class TokenController extends AbstractBaseController
{
    /**
     * @var JwtTokenServiceInterface
     */
    private $tokenService;

    public function __construct(
        JwtTokenServiceInterface $tokenService,
        SerializerInterface $serializer
    )
    {
        parent::__construct($serializer);
        $this->tokenService = $tokenService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function createToken(Request $request)
    {
        $token = $this->tokenService->createToken($request);

        return $this->response($token, 201);
    }
}
