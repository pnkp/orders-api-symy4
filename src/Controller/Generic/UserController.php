<?php

namespace App\Controller\Generic;

use App\Common\Dto\User\UserRequestDto;
use App\Common\Serializer\SerializerInterface;
use App\Common\Service\User\UserServiceInterface;
use App\Controller\AbstractBaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * @Security("is_authenticated()")
 * Class UserController
 * @package App\Controller\Generic
 */
class UserController extends AbstractBaseController
{
    /**
     * @var UserServiceInterface
     */
    private $userService;

    public function __construct(
        UserServiceInterface $userService,
        SerializerInterface $serializer
    )
    {
        parent::__construct($serializer);
        $this->userService = $userService;
    }

    /**
     * @ParamConverter(name="body_content_converter", class="App\Common\Dto\User\UserRequestDto")
     *
     * @param UserRequestDto $userRequestDto
     * @return JsonResponse
     */
    public function createUser(UserRequestDto $userRequestDto)
    {
        return $this->response($this->userService->createUser($userRequestDto), 201);
    }
}
