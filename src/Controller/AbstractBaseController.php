<?php

namespace App\Controller;

use App\Common\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

abstract class AbstractBaseController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function response($data, int $statusCode): JsonResponse
    {
        $serializedData = $this->serializer->serialize($data, 'json');
        return JsonResponse::fromJsonString($serializedData, $statusCode);
    }
}
