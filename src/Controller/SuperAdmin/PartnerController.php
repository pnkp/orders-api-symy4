<?php

namespace App\Controller\SuperAdmin;

use App\Common\Dto\Partner\PartnerRequestDto;
use App\Common\Service\Partner\PartnerServiceInterface;
use App\Controller\AbstractBaseController;
use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Common\Serializer\SerializerInterface as JsonSerializer;

class PartnerController extends AbstractBaseController
{
    /**
     * @var PartnerServiceInterface
     */
    private $partnerService;

    public function __construct(
        PartnerServiceInterface $partnerService,
        JsonSerializer $jsonSerializer
    )
    {
        parent::__construct($jsonSerializer);
        $this->partnerService = $partnerService;
    }

    /**
     * @ParamConverter(name="body_content_converter", class="App\Common\Dto\Partner\PartnerRequestDto")
     * @param PartnerRequestDto $partnerRequestDto
     * @return JsonResponse
     */
    public function createPartner(PartnerRequestDto $partnerRequestDto): JsonResponse
    {
        $this->denyAccessUnlessGranted(User::ROLE_SUPER_ADMIN);
        return $this->response($this->partnerService->create($partnerRequestDto), 201);
    }

    /**
     * @ParamConverter(name="body_content_converter", class="App\Common\Dto\Partner\PartnerResponseDto")
     * @param int $id
     * @return JsonResponse
     */
    public function getPartner(int $id)
    {
        $this->denyAccessUnlessGranted(User::ROLE_SUPER_ADMIN);
        return $this->response($this->partnerService->findPartnerById($id), 200);
    }

    public function getPartnerByQuery(Request $request)
    {
        $this->denyAccessUnlessGranted(User::ROLE_SUPER_ADMIN);
        return $this->response($this->partnerService->findPartnerByCriteria($request->query->all()), 200);
    }
}
