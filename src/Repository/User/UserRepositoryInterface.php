<?php

namespace App\Repository\User;

use App\Common\Dto\User\UserRequestDto;
use App\Entity\User;

interface UserRepositoryInterface
{
    public function create(UserRequestDto $userRequestDto): User;

    public function findOneByCriteria(array $criteria): ?User;

    public function findAll(): array;
}