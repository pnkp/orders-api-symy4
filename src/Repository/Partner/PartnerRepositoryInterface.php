<?php

namespace App\Repository\Partner;

use App\Entity\Partner;
use App\Repository\GenericRepositoryInterface;

interface PartnerRepositoryInterface extends GenericRepositoryInterface
{
    public function findOneByCriteria(array $criteria): ?Partner;

    public function createPartner(Partner $partner): Partner;

    public function findOneById(int $id): ?Partner;
}
