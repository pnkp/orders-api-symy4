<?php

namespace App\Repository\Partner;

use App\Entity\Partner;
use Doctrine\ORM\EntityManagerInterface;

class PartnerRepository implements PartnerRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function findAll(): array
    {
        return $this->entityManager->getRepository(Partner::class)->findAll();
    }

    public function findOneByCriteria(array $criteria): ?Partner
    {
        return $this->entityManager->getRepository(Partner::class)->findOneBy($criteria);
    }

    public function createPartner(Partner $partner): Partner
    {
        $this->entityManager->persist($partner);
        $this->entityManager->flush();

        return $partner;
    }

    public function findOneById(int $id): ?Partner
    {
        return $this->entityManager->getRepository(Partner::class)->find($id);
    }
}
